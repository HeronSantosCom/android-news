/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function($){
    
    var AdConfig = false;
    
    var AdLocation = false;
    
    var AdTitle = null;
    
    var AdNewsFeed = new Array();
    var AdNewsStarted = false;
    var AdNewsLength = 0;
    var AdNewsThis = 0;
    var AdNewsTime = false;
    
    var AdDisplayFeed = new Array();
    var AdDisplayStarted = false;
    var AdDisplayLength = 0;
    var AdDisplayThis = 0;
    var AdDisplayNext = null;
    var AdDisplayTime = false;
    
    $.fn.getElementExists = function(){
        return this.length>0;
    };
    
    $.fn.setCookie = function(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    };
    
    $.fn.getCookie = function(c_name) {
        var i, x, y, ARRcookies = document.cookie.split(";");
        for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
            x = x.replace(/^\s+|\s+$/g, "");
            if (x == c_name) {
                return unescape(y);
            }
        }
        return false;
    };
    
    $.fn.setElement = function(options) {
        var defaults = {
            element: false,
            id: false,
            style: false,
            property: [],
            content: false
        }
        options = $.extend(defaults, options);
        if (options.element !== false) {
            var element = '<' + options.element + ' ';
            if (options.id !== false) {
                element = element + 'id="' + options.id + '" ';
            }
            if (options.style !== false) {
                element = element + 'class="' + options.style + '" ';
            }
            if (options.property.length > 0) {
                $(options.property).each(function() {
                    var property_nome = this[0];
                    if (this[1] != undefined) {
                        var property_valor = this[1];
                        element = element + property_nome + '="' + property_valor + '" ';
                    }
                });
            }
            if (options.content !== false) {
                element = element + '>' + options.content + '</' + options.element + '>';
            } else {
                element = element + '/>';
            }
            if (options.input)
                return element;
            $(this).append(element);
        }
    };
    
    $.fn.getLocation = function() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {  
                AdLocation = position;
            }, function (error) {
                switch(error.code) {
                    case error.TIMEOUT:
                        console.log ('navigator.geolocation = Timeout');
                        break;
                    case error.POSITION_UNAVAILABLE:
                        console.log ('navigator.geolocation = Position unavailable');
                        break;
                    case error.PERMISSION_DENIED:
                        console.log ('navigator.geolocation = Permission denied');
                        break;
                    case error.UNKNOWN_ERROR:
                        console.log ('navigator.geolocation = Unknown error');
                        break;
                }
            });
        }
    };
    
    $.fn.setTime = function() {
        var momentoAtual = new Date(); 
        
        var semana = momentoAtual.getDay();
        
        var dia = momentoAtual.getDate();
        var mes = momentoAtual.getMonth();
        var ano = momentoAtual.getFullYear();
        
        var hora = momentoAtual.getHours(); 
        var minuto = momentoAtual.getMinutes();
        var segundo = momentoAtual.getSeconds();
        
        if (dia <= 9) {
            dia = "0"+dia;	
        }
        if (segundo <= 9) {
            segundo = "0"+segundo;	
        }
        if (minuto <= 9) {
            minuto = "0"+minuto;	
        }
        if (hora <=9 ) {
            hora = "0"+hora;	
        }
        
        switch(semana) {
            case 0:
                semana = "Domingo";
                break;
            case 1:
                semana = "Segunda-feira";
                break;
            case 2:
                semana = "Terça-feira";
                break;
            case 3:
                semana = "Quarta-feira";
                break;
            case 4:
                semana = "Quinta-feira";
                break;
            case 5:
                semana = "Sexta-feira";
                break;
            case 6:
                semana = "Sábado";
                break;
            case 7:
                semana = "Domingo";
                break;
        }
	
        switch(mes) {
            case 0:
                mes = "Jan";
                break;
            case 1:
                mes = "Fev";
                break;
            case 2:
                mes = "Mar";
                break;
            case 3:
                mes = "Abr";
                break;
            case 4:
                mes = "Mai";
                break;
            case 5:
                mes = "Jun";
                break;
            case 6:
                mes = "Jul";
                break;
            case 7:
                mes = "Ago";
                break;
            case 8:
                mes = "Set";
                break;
            case 9:
                mes = "Out";
                break;
            case 10:
                mes = "Nov";
                break;
            case 11:
                mes = "Dez";
                break;
        }
        
        
        if (!$("#hora").getElementExists()) {
            $("#barra_relogio").setElement({
                element: 'div',
                id: 'hora',
                content: '00:00'
            });
        }
        $("#hora").html(hora +":"+minuto);
    
        if (!$("#dia").getElementExists()) {
            $("#barra_relogio").setElement({
                element: 'div',
                id: 'dia',
                content: 'Domingo'
            });
        }
        $("#dia").html(semana);
    
        if (!$("#data").getElementExists()) {
            $("#barra_relogio").setElement({
                element: 'div',
                id: 'data',
                content: '01/Jan/2012'
            });
        }
        $("#data").html(dia + "/" + mes + "/" + ano);
        
        setTimeout(function() {
            $(this).setTime();
        },60000);
    };

    $.fn.Connection = function(funcao, sucesso, erro) {
        var pin = $(this).getCookie("pin");
        var uuid = $(this).getCookie("uuid");
        if (pin.length > 0) {
            $.ajax({
                type: "POST",
                url: 'http://ad.guiariosul.com.br/ws/',
                dataType: 'json',
                data: {
                    funcao: funcao,
                    pin: pin,
                    uuid: uuid
                },
                success: sucesso,
                error: erro
            });
        }
    }
    
    $.fn.Ad_Init = function() {
        $(this).AdPin_Check();
    };
    
    $.fn.AdPin_OpenBox = function(pinmsg) {
        $(this).setCookie("pin", "", 365);
        if (!$("#pin").getElementExists()) {
            $("body").setElement({
                element: 'div',
                id: 'pin'
            });
            $("#pin").setElement({
                element: 'div',
                id: 'pinbox'
            });
            $("#pinbox").setElement({
                element: 'form',
                id: "pinform",
                property: [["method", "POST"]]
            });
            $("#pinform").setElement({
                element: 'div',
                id: 'pinmsg',
                content: pinmsg
            });
            $("#pinform").setElement({
                element: 'div',
                id: "pintitle",
                content: 'PIN:'
            });
            $("#pinform").setElement({
                element: 'input',
                id: "pintext",
                property: [["type", "password"], ["name", "pin"], ["action", ""], ["maxlength", "4"]]
            });
            $("#pinform").submit(function() {
                $(this).AdPin_Check();
                return false;
            });
            $('#pintext').keypress(function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    return false;
                } 
            });
            $('#pintext').blur(function(){
                var pin = $('#pintext').val();
                if (pin.length > 0) {
                    if (pin.length == 4) {
                        var agree = confirm("Confirmar autenticação?");
                        if (agree) {
                            $(this).setCookie("pin", pin, 365);
                            $("#pin").hide();
                            $('#pintext').val("");
                            $("#pinform").trigger("submit");
                        }
                    }
                }
            });
        } else {
            $("#pin").show();
            $("#pinmsg").html(pinmsg);
        }
    }
    
    $.fn.AdPin_Check = function() {
        var pin = $(this).getCookie("pin");
        if (pin.length > 0) {
            $(this).Connection("pin", function(data) {
                if (data.status !== false) {
                    data = data.data;
                    AdTitle = data.name;
                    $(this).setCookie("uuid", data.device.uuid, 365);
                    $(this).AdNews_Update();
                    $(this).AdDisplay_Update();
                } else {
                    if (data.data == 300) {
                        $(this).setCookie("uuid", "", 365);
                        $(this).AdPin_Check();
                    } else {
                        $(this).AdPin_OpenBox("O PIN inserido não é válido!");
                    }
                }
            }, function(xhr) {
                console.log(xhr);
                $(this).AdPin_OpenBox("Não foi possível processar seu PIN,\nTente novamente mais tarde!");
            });
        } else {
            $(this).AdPin_OpenBox("O PIN inserido não é válido!");
        }
    };
    
    $.fn.AdNews = function() {
        if (AdNewsStarted == true) {
            if (AdNewsThis >= AdNewsLength) 
                AdNewsThis = 0;
            $(this).AdNews_Changer(AdNewsFeed[AdNewsThis]);
            AdNewsThis++;
        } else {
            if ($("#fonte").getElementExists()) {
                $('#barra_noticia').slideUp('slow');
                $('#barra_noticia').queue(function() {
                    $('#barra_noticia').html("<div id='noticia'>Carregando...</div>");
                    $('#barra_noticia').slideDown('slow');
                    $(this).dequeue();
                });
            }
        }
        AdNewsTime = setTimeout(function() {
            $(this).AdNews();
        }, 5000);
    };
    
    $.fn.AdNews_Update = function() {
        AdNewsStarted = false;
        $(this).Connection("news", function(data) {
            if (data.status !== false) {
                data = data.data;
                var AdNews_Pointer = 0;
                $(data).each(function() {
                    AdNewsFeed[AdNews_Pointer] = [this.titulo, this.fonte];
                    AdNews_Pointer++;
                });
                AdNewsLength = AdNewsFeed.length;
                if (AdNewsLength > 0) {
                    AdNewsStarted = true;
                }
            }
        }, function(xhr) {
            console.log(xhr);
        });
        setTimeout(function() {
            $(this).AdNews_Update();
        }, 18000000); //300000
    };
    
    $.fn.AdNews_Changer = function (noticia) {
        $('#barra_noticia').slideUp('slow');
        $('#barra_noticia').queue(function() {
            $('#barra_noticia').html("<div id='noticia'>" + noticia[0] + "<div id='fonte'>" + noticia[1] + "</div></div>");
            $('#barra_noticia').slideDown('slow');
            $(this).dequeue();
        });
    };
    
    $.fn.AdTimeout = function(AdDisplayThis) {
        if (AdDisplayThis == 0) {
            if (!$("#barra_topo").getElementExists()) {
                $("body").setElement({
                    element: 'div',
                    id: 'barra_topo',
                    content: '<div>'+ AdTitle +'</div>'
                });
                $("body").setElement({
                    element: 'div',
                    id: 'barra_relogio'
                });
                $("body").setElement({
                    element: 'div',
                    id: 'barra_noticia',
                    content: '<div id="noticia">Carregando...</div>'
                });
            }
            if (AdNewsLength > 0) {
                AdNewsStarted = true;
            }
            $(this).setTime();
            $(this).AdNews();
        } else {
            if ($("#barra_topo").getElementExists()) {
                $("#barra_topo, #barra_relogio, #barra_noticia").hide().remove();
                AdNewsStarted = false;
            }
        }
    }

    $.fn.AdDisplay = function(InLooping) {
        console.log('$(this).AdDisplay('+InLooping+')');
        var AdDisplayTimeout = 1000;
        if (AdDisplayStarted == true) {
            if (AdDisplayThis >= AdDisplayLength) 
                AdDisplayThis = 0;
            console.log(AdDisplayFeed[AdDisplayThis]);
            AdDisplayTimeout = AdDisplayFeed[AdDisplayThis].timeout;
            if (InLooping == true) {
                api.nextSlide();
            }
            if (AdDisplayThis == 0) {
                $(this).AdTimeout(AdDisplayThis);
            }
            AdDisplayThis++;
        }
        AdDisplayTime = setTimeout(function() {
            $(this).AdDisplay(true);
        }, AdDisplayTimeout);
    };
    
    $.fn.AdDisplay_Update = function() {
        console.log('$(this).AdDisplay_Update()');
        AdDisplayStarted = false;
        $(this).Connection("ads", function(data) {
            console.log('$(this).Connection(ads)');
            console.log(data);
            if (data.status !== false) {
                data = data.data;
                var AdDisplay_Pointer = 0;
                AdDisplayFeed[AdDisplay_Pointer] = {
                    image : "wallpaper.jpg",
                    timeout: 60000
                };
                AdDisplay_Pointer++;
                $(data).each(function() {
                    AdDisplayFeed[AdDisplay_Pointer] = {
                        image : this.imagem,
                        timeout: this.timeout
                    };
                    AdDisplay_Pointer++;
                });
                AdDisplayLength = AdDisplayFeed.length;
                console.log(AdDisplayFeed);
                console.log(AdDisplayLength);
                if (AdDisplayLength > 0) {
                    $.supersized({
                        fit_landscape: true,
                        autoplay : false,
                        transition : 3,
                        slides : AdDisplayFeed
                    });
                    AdDisplayStarted = true;
                    $(this).AdDisplay(false);
                }
            }
        }, function(xhr) {
            console.log(xhr);
        });
        setTimeout(function() {
            $(this).AdDisplay_Update();
        }, 86400000);
    };
    
    $(document).ready(function() {
        
        $(this).getLocation();
        window.onload = function(){
            setTimeout(function() {
                $(this).Ad_Init();
                window.scrollTo(0, 1);
            }, 1);
        };


    });
    
})(jQuery);