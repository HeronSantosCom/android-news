<?php

//SOURCEBY: http://www.yazu.be

require_once ('/home/karroos_1/sites.controle/core/core.php');

//header("Content-Type: text/html; charset=UTF-8 language=pt-BR");
//$link_topo = '<strong>Visitante!</strong> [ <a href="/cadastro">Cadastre-se</a> | <a href="/entrar">Entrar</a> ]';
//if(isset($_SESSION['user']) && count($_SESSION['user']) > 0){
//$link_topo = '<strong>'.$_SESSION['user']['name'].'</strong> [ <a href="/painel">Meu Painel</a> | <a href="/usuario/?acao=sair">Sair</a> ]';
//}
//$menu_cidades = null;
//$qry_cidades = $conexao->seleciona('`mococa_guia`.`cidades`', "id, nome, link");
//if (is_array($qry_cidades)) {
//    $count = 1;
//    foreach ($qry_cidades as $aux_cidades) {
//        $menu_cidades[] = '<ul><li class="maincat cat-item-'.$aux_cidades['id'].'"><a href="http://'.$_CONFIG['url'].'/cidades/'.$aux_cidades['link'].'" title="">'.$aux_cidades['nome'].'</a></li></ul>';
//        if($count > 8){
//            $menu_cidades[] = '</div><!-- /catcol --><div class="catcol  first">';
//            $count = 1;
//        } else {
//            $count++;
//        }
//    }
//    $menu_cidades = join(null, $menu_cidades);
//    unset($aux_cidades);
//}
//unset($qry_cidades);
// $_CONFIG['name'] 

function checkPin($pin, $uuid) {
    global $conexao;
    $device = $conexao->seleciona('`mococa_ads`.`device`', null, "pin = '{$pin}'");
    $json_data = 100;
    if (is_array($device[0])) {
        if ($device[0]['uuid'] == $uuid) {
            return true;
        }
    }
    return false;
}

$json_status = $json_data = false;
$funcao = (isset($_POST["funcao"]) ? $_POST["funcao"] : false);
$pin = (isset($_POST["pin"]) ? $_POST["pin"] : false);
$uuid = (isset($_POST["uuid"]) ? $_POST["uuid"] : false);
switch ($funcao) {
    case "pin":
        //seleciona($tabela = null, $campos = null, $condicao = null, $ordem = null, $outros = null)
        $device = $conexao->seleciona('`mococa_ads`.`device`', null, "pin = '{$pin}'");
        $json_data = 100;
        if (is_array($device[0])) {
            if (strlen($device[0]['uuid']) == 0 && strlen($uuid) == 0) {
                $device[0]['uuid'] = $uuid = session_id();
                $conexao->altera('`mococa_ads`.`device`', "uuid = '{$uuid}'", "pin = '{$pin}'");
            }
            $json_data = 200;
            if (strlen($device[0]['uuid']) == 0) {
                $json_data = 300;
            }
            if ($device[0]['uuid'] == $uuid) {
                $json_status = true;
                $json_data = array_merge($_CONFIG, array("device" => $device[0]));
            }
        }
        break;
    case "news":
        $json_data = 100;
        if (checkPin($pin, $uuid)) {
            $json_data = 200;
            $news = $conexao->seleciona('`mococa_ads`.`news`', null, "status = '1'", "pubDate DESC", "LIMIT 20");
            if (is_array($news[0])) {
                $json_data = false;
                $json_status = true;
                foreach ($news as $new) {
                    $json_data[] = array("id" => $new["id"], "titulo" => utf8_encode($new["title"]), "fonte" => utf8_encode($new["source"]));
                }
            }
        }
        break;
    case "ads":
        $json_data = 100;
        if (checkPin($pin, $uuid)) {
            $json_data = 200;
            $ads = $conexao->seleciona('`mococa_ads`.`ads`', null, "status = '1' and DATE(NOW()) <= DATE(dt_validate)");
            if (is_array($ads[0])) {
                $json_data = false;
                $json_status = true;
                foreach ($ads as $ad) {
                    $json_data[] = array("id" => $ad["id"], "imagem" => $ad["image"], "timeout" => $ad["timeout"]);
                }
            }
        }
        break;
}
die(json_encode(array("status" => $json_status, "data" => $json_data)));
?>